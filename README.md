This tool takes an input image or video feed and transforms it for use on a custom Pepper's Ghost 3D holographic display.

Authors:
Aidan McDonald & Joe Gambetta

Required Python Libraries:
PyQt5
fbs
OpenCV
