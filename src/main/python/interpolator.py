import cv2 as cv
import numpy as np
import math

class Interpolator():
    def __init__:
        self.interpolationPts = {}
        for i in range(5):
            for j in range(5):
                self.interpolationPts[(i, j)] = (i, j)

    def interpolate(pt):
        x0 = math.floor(pt[0])
        x1 = math.ceil(pt[0])
        xScale = pt[0] - x0
        y0 = math.floor(pt[1])
        y1 = math.ceil(pt[1])
        yScale = pt[1] - y0
        xVal = xScale*self.interpolationPts[(x0, y0)][0] + (1-xScale)*self.interpolationPts[x1, y1][0]
        yVal = yScale*self.interpolationPts[(x0, y0)][0] + (1-yScale)*self.interpolationPts[x1, y1][0]
        return (xVal, yVal)
