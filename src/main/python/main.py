from fbs_runtime.application_context.PyQt5 import ApplicationContext
from PyQt5.QtWidgets import QMainWindow
from PyQt5 import QtCore, QtGui, QtWidgets
import cv2 as cv
import numpy as np

import sys

# Controls for the display of the image on screen
maxWidthPct = 0.75
maxHeightPct = 0.25
rOffsetPct = 0.05

class ImgViewer(QtWidgets.QMainWindow):
  def __init__(self, parent=None):
    QtWidgets.QMainWindow.__init__(self, parent=parent)

    sizeObject = QtWidgets.QDesktopWidget().screenGeometry(-1)
    imH = sizeObject.height()*3
    imW = sizeObject.width()*3

    dialog = QtWidgets.QFileDialog(None, self.tr("Open Image"), "~/", self.tr("Images (*.png *.jpg)"))
    status = dialog.exec()
    self.inFileName = ""
    if status:
        self.inFileName = dialog.selectedFiles()[0]
        dispImg = cv.imread(self.inFileName)
        baseImg = np.zeros((imH,imW,3), np.uint8)

        dispImg = cv.rotate(dispImg, cv.ROTATE_90_CLOCKWISE)
        # If image is portrait
        if dispImg.shape[0] > dispImg.shape[1]:
            newHeight = imH * maxWidthPct
            scaleFactor = newHeight / dispImg.shape[0]
            newWidth = scaleFactor * dispImg.shape[1]
        else:
            newWidth = imW * maxHeightPct
            scaleFactor = newWidth / dispImg.shape[1]
            newHeight = scaleFactor * dispImg.shape[0]
        dispImg = cv.resize(dispImg, (int(newWidth), int(newHeight)))

        x0 = int(imW*(1 - rOffsetPct) - dispImg.shape[1])
        x1 = x0 + dispImg.shape[1]
        y0 = int((imH - dispImg.shape[0]) / 2)
        y1 = y0 + dispImg.shape[0]

        baseImg[y0:y1, x0:x1] = dispImg

        cv.imshow("Display window", baseImg)
        k = cv.waitKey(0)


if __name__ == '__main__':
    appctxt = ApplicationContext()
    window = ImgViewer()
    #window.show()
    #exit_code = appctxt.app.exec_()

    sys.exit(0)
